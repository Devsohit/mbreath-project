#include<stdio.h>

void convolution(long *Sig,double *Fltr, int Fltr_len,
                                   double *op, long op_len)
{
	FILE  *fp;
    for (int i = 0; i < op_len; i++)
    {
        double sum = 0;
        for (int j = 0; j < Fltr_len; j++)
            sum += Sig[i+j] * Fltr[Fltr_len - 1 - j] ;  // simple convolution 
        op[i] = sum;                                    // to average the samples
    }

	
fp=fopen("output.csv","w");  // storing all output samples in file
    for (int i = 0; i < op_len; i++ ){ 
	   fprintf(fp,"%lf ",op[i]); 
	  // printf("%lf ",op[i]);
	   } 
    printf("\n");
    fclose(fp);
}
void main()
{
	int i,j,k,v;	
	int T=600,fs=100;
	long int y[T*fs];

	double filt[50],op[T*fs],v1,**opf;
	FILE *fp1,*fp2,*fp3;

	fp1 = fopen("signal.csv","r");
	fp2 = fopen("filt_cof.csv","r");


	for(i =0, k=0 ; i< T; i++)
		for (j=0 ; j< fs; j++)
		{
			fscanf(fp1,"%d",&v);
			//printf("%f",v);
			y[k++]=v;         // storing all siganl coeff. in buffer Y.
			v=0;
		}

	fclose(fp1);

	for (k=0,j=0 ; j< 50; j++)
	{
		fscanf(fp2,"%lf",&v1);
		//printf("%f",v);
		filt[k++]=v1; // storing all filter coeff. in buffer filt.
		v1=0;
	}


	fclose(fp2);
convolution(y,filt,50,op,T*fs);  // calling the convolution function

}